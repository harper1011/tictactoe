Name:       tictactoe-restful-backend
Version:    0.1
Release:    1

Source0:    tictactoe.service
Source1:    tictactoe_restful_backend.py
Source2:    test_tictactoe_restful_backend.py


Summary:    TicTacToe RESTful Backend
BuildArch:  noarch
License:    BSD


%define cwd %(pwd)
%define _app tictactoe

%description
%{summary}

%clean
rm -rf %{buildroot}

%build
install -d %{buildroot}/opt/script/tictactoe/
install %{S:1} %{S:2} %{buildroot}/opt/script/tictactoe/

install -d %{buildroot}%{_unitdir}
install %{S:0} %{buildroot}%{_unitdir}

%files
%defattr(644, root, root, 755)
%attr(755, root, root) /opt/script/tictactoe/*.py
%config %{_unitdir}/*.service

%post
systemctl daemon-reload
systemctl enable %{_app}

%postun
systemctl stop %{_app}
systemctl disable %{_app}
systemctl daemon-reload

%changelog
* Sat Oct 07 2017 Dapeng Jiao <harper1011@gmail.com> - %{version}-%{release}
- initial build
