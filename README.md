Tictactoe RESTful Backend
====
Tictactoe RESTful Backend for demo purpose only:

The RESTful Backend need to be accessed via following URL:
http://$SERVER_IP:3000/api/v1/games/
As for demo purpose, there is only v1 available. All other undefined URL access will return with HTTP Status Code 404

There are some examples:
* Listing of games
    * URL: "http://$SERVER_IP:3000/api/v1/games/"
    * REQUEST_METHOD: "GET"
* Viewing a single game
    * URL: "http://$SERVER_IP:3000/api/v1/games/$game_uuid"
    * REQUEST_METHOD: "GET"
* Creating a new game
    * URL: "http://$SERVER_IP:3000/api/v1/games/"
    * REQUEST_METHOD: "POST"
* Deleting a game
    * URL: "http://$SERVER_IP:3000/api/v1/$game_uuid"
    * REQUEST_METHOD: "DELETE"
* Continue a game
    * URL: "http://$SERVER_IP:3000/api/v1/$game_uuid"
    * REQUEST_METHOD: "PUT"

--------------------------------------------------------------------------------------------------
There are two ways to test it:

* Build this project into rpm package and install it, then access via ***localhost*** as host IP.

    * Step by step:
        * Use provided Vagrantfile to init a CentOS VM
        
            ```
            # vagrant up
            ```
        * Log into Vagrant environment and then move to this project folder and make sure all project files can be seen from this folder
        
            ```
            # vagrant ssh
            
            $ cd /vagrant/
            $ ls
            Dockerfile  Makefile  README.md  SOURCES  SPECS  test.log  tictactoe.txt  tictactoe.yaml  Vagrantfile
            ```
        * Compile project into RPM package
        
            ```
            $ make clean rpm
            ```
        * Install rpm file then start ***tictactoe*** service and make sure ***tictactoe*** service working properly
        
            ```
            $ sudo rpm -ivh --replacepkgs tictactoe-restful-backend-*.rpm --force
            $ sudo systemctl start tictactoe
            $ systemctl status tictactoe
            ```
        * Send your own REST query to *http://localhost:3000/api/v1* or run Module Testcase with following command to see whether the tictactoe backendis working properly
        
            ```
            $ python SOURCES/test_tictactoe_restful_backend.py
            ...
            .
            ----------------------------------------------------------------------
            Ran 12 tests in 2.604s
            
            OK
            ```

    * All in one script:
        * Use provided Vagrantfile to init a CentOS VM.
        * Log into Vagrant environment and then move to this project folder and make sure all project files can be seen from this folder
        * Execute command in order to make auto setup which is the same as *step by step* section
        
            ```
            $ python scripts/all-in-one.py
            ```
        * Send your own REST query to *http://localhost:3000/api/v1/games/* or run Module Testcase with following command to see whether the tictactoe backendis working
        
            ```
            $ python SOURCES/test_tictactoe_restful_backend.py
            ...
            .
            ----------------------------------------------------------------------
            Ran 12 tests in 2.604s
            
            OK
            ```

* Run ***Tictactoe RESTful Backend*** in docker containers, then access via its container's host IP.

    * Step by step
        * Use provided Vagrantfile to init a CentOS VM.
        * Log into Vagrant environment and then move to this project folder and make sure all project files can be seen from this folder
        * Build ***Tictactoe RESTful Backend*** into docker image
        
            ```
            $ sudo docker build -t tictactoe .
            ```
        * Remove old containers if exist
            ```
            $ sudo docker rm $(sudo docker ps -a -q) -f
            ```
        * Stop *tictactoe* and *redis* service which may already running during previous testing steps
            ```
            $ sudo systemctl stop tictactoe
            $ sudo systemctl stop redis
            ```
        * Run *RedisDB* service as a container
            ```
            $ sudo docker run -d --name redis -p 6379:6379 redis
            ```
        * Run ***tictactoe*** servince in another container and link with existing ***RedisDB*** container
            ```
            $ sudo docker run -p 3000:3000 --name tictactoe -d --link redis:redis tictactoe
            ```
        * After at least 10 seconds, check the status of containers, and make sure container named as ***tictactoe*** health state is ***heathly***

            ```
            $ sudo docker ps --format "table {{.Image}}\t{{.Status}}\t{{.Ports}}\t{{.Names}}"
            IMAGE               STATUS                   PORTS                    NAMES
            tictactoe           Up 5 minutes (healthy)   0.0.0.0:3000->3000/tcp   tictactoe
            redis               Up 5 minutes             0.0.0.0:6379->6379/tcp   redis
            ```
        * Check the ***tictactoe*** container host IP and assign it to environment variable        
            ```
            $ tictactoe_service_ip=$(sudo docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' tictactoe)
            $ echo $tictactoe_service_ip
            172.17.0.3
            ```
        * Execute testcase from local VM towards to REST API running on container
        
            ```
            $ python SOURCES/test_tictactoe_restful_backend.py $tictactoe_service_ip
            ...
            .
            ----------------------------------------------------------------------
            Ran 12 tests in 2.717s
            
            OK
            ```
         * Or if you want you can also execute testcase from another container
         
            ```
            $ sudo docker run -it --rm --name tester --link tictactoe:tictactoe -v "$PWD"/SOURCES/:/opt/script/tictactoe/ \
                python:2 /bin/bash -c "pip install -r /opt/script/tictactoe/requirements.txt && python /opt/script/tictactoe/test_tictactoe_restful_backend.py $tictactoe_service_ip"
            ...
            .
            ----------------------------------------------------------------------
            Ran 12 tests in 4.238s
            
            OK
            ```
    * Docker all in one script:
        * Use provided Vagrantfile to init a CentOS VM (recommend to destroy the previous one).
        * Log into Vagrant environment and then move to this project folder and make sure all project files can be seen from this folder
        * Execute script in order to make container to be created and executed the same as *step by step* section include testing as well.
            
            ```
            $ python scripts/docker-all-in-one.py
            ...
            .
            ----------------------------------------------------------------------
            Ran 12 tests in 5.345s
            
            OK
            ```


In case there is any problem during your verification, please contact me via email.

BTW: There is a ".gitlab-ci.yml" which is used for auto CI&CD testing. In case you use GitLab you can use that as well. [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)
