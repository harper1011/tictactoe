#!/usr/bin/env python
import json
import logging
import os
import pprint
import sys
import uuid
from collections import OrderedDict
from random import randint

import bottle
import redis

APP_NAME = 'tictactoe_restful_backend'
FORMAT = '%(asctime)s %(module)s:%(lineno)-3s[%(levelname)s] %(message)s'
ROOT_URL_PATH = '/api/v1/games/'
TEST_URL_PATH = '/tst/v1/games/'

logging.basicConfig(format=FORMAT, level=logging.WARN)
logger = logging.getLogger(APP_NAME)
logger.setLevel(logging.INFO)

REDIS_IP = os.getenv('REDIS_PORT_6379_TCP_ADDR', 'localhost')
REDIS_PORT = os.getenv('REDIS_PORT_6379_TCP_PORT', 6379)
redis_client = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0)
GAME_BOARD_STATUS = 'board'
USER_SYMBOL = 'user_symbol'
STATUS = 'status'
NOUGHTS = 'O'
CROSSES = 'X'
EMPTY = '-'
DEFAULT_EMPTY_INPUT = '---------'
MAX_ALLOW_DATA_LENGTH = 9
RUNNING = 'RUNNING'
X_WON = 'X_WON'
O_WON = 'O_WON'
DRAW = 'DRAW'

server_default_symbol = CROSSES

try:
    redis_client.info()
    logger.info('Connect to RedisDB successfully. Use RedisDB for storing data.')
    _fake_db = None
except Exception as e:
    logger.error("Failed to connect to RedisDB due to: %s, Use internal cache dictionary for storing data.", e.message)
    _fake_db = OrderedDict()


def validate_input(data):
    if data != '' and len(data) != MAX_ALLOW_DATA_LENGTH:
        logger.error('Invalid input data length. Expected: %s, Received %s', MAX_ALLOW_DATA_LENGTH, len(data))
        return False
    if len(str(data).translate(None, '%s%s%s' % (NOUGHTS, CROSSES, EMPTY))) > 0:
        logger.error('Contain invalid move symbol: %s', str(data).translate(None, '%s%s%s' % (NOUGHTS, CROSSES, EMPTY)))
        return False
    return True


def user_start_game(data):
    if not data:
        # if empty. then its server start the game
        logger.debug('No given game borad data --> server start')
        return 0
    elif not str(data).translate(None, EMPTY):
        # if nothing than '-'. also consider as server start the game
        logger.debug('No given game borad data --> server start')
        return 0
    elif len(str(data).translate(None, EMPTY)) > 1:
        # if more than 1 given symbol, then return 500
        logger.debug('More than one move is given, termiate with 500')
        return 500
    else:
        # return user given symbol
        user_symbol = str(data).translate(None, EMPTY).upper()
        logger.debug('User start game with %s', user_symbol)
        return user_symbol


def server_step(data, user_symbol):
    # simple server step, assign to a random step
    data_list = list(data)
    new_move_idx = randint(0, 8)
    while data_list[new_move_idx] != EMPTY:
        new_move_idx = randint(0, 8)
    data_list[new_move_idx] = NOUGHTS if user_symbol == CROSSES else CROSSES
    return ''.join(data_list), new_move_idx


def find_diff_count(old_game_data, new_game_data):
    diff_count = 0
    changed_idx = 0
    current_idx = 0
    user_changed_symbol = ''
    for old, new in zip(old_game_data, new_game_data):
        if old != new:
            diff_count += 1
            # if new changed symbol is different than existing one, then set user changed symbol to None and handle as an error
            user_changed_symbol = new if (user_changed_symbol == '' or user_changed_symbol == new) else None
            if old != EMPTY:
                # if old place is not empty, return 0 as error handling
                logger.error('New move to a place which was not empty. Aborting......')
                return 0, None, changed_idx
            elif new != NOUGHTS and new != CROSSES:
                # if new place is not valid input, return 0 as error handling
                logger.error('New move symbol is not valid. Aborting......')
                return 0, None, changed_idx
            else:
                changed_idx = current_idx
        current_idx += 1
    logger.debug('%s new move with changed symbol %s', diff_count, user_changed_symbol)
    return diff_count, user_changed_symbol, changed_idx


def validate_new_input(old_game_data, new_game_data):
    if old_game_data == new_game_data:
        # when no changed
        logger.error('No move detected!')
        return False, None, None
    diff_count, user_changed_symbol, user_changed_idx = find_diff_count(old_game_data, new_game_data)
    if diff_count == 0:
        # For invalid input
        logger.error('Invalid move detected!')
        return False, user_changed_symbol, None
    elif diff_count > 1:
        # only 1 different is allowed
        logger.error('More than one move detected!')
        return False, user_changed_symbol, None

    logger.debug('New move at index %s, with symbol %s', user_changed_idx, user_changed_symbol)
    return True, user_changed_symbol, user_changed_idx


def check_winner(game_data, new_move_symbol, new_move_idx):
    game_list = list(game_data)
    except_key_list = [
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (2, 4, 6)
    ]
    for each in except_key_list:
        try:
            if game_list[each[0]] == game_list[each[1]] == game_list[each[2]] == new_move_symbol:
                return True
        except Exception as e:
            logger.error('Exception: %s during check_winner. Return False', e.message)
            return False
    return False


@bottle.route('/:#.*#', method='ANY')
def default_action():
    bottle.response.status = 404
    return


@bottle.get(ROOT_URL_PATH)
def list_action():
    try:
        bottle.response.status = 200
        if _fake_db is None:  # redis in use
            keys = redis_client.keys()
            vals = [json.loads(redis_client.get(x)) for x in keys]
            logger.debug('list all: keys: %s, vals: %s', keys, vals)
            return dict(zip(keys, vals))
        else:
            return _fake_db
    except Exception as e:
        logger.exception('Exception during handling GET ALL action')
        bottle.response.status = '500 Exception: %s during handling GET ALL action' % e.message
        return


@bottle.get(ROOT_URL_PATH + '<idx>')
def get_action(idx):
    logger.debug('GET: %s', idx)
    if _fake_db is None:  # redis in use
        val = redis_client.get(idx)
        if val:
            bottle.response.status = 200
            return json.loads(val)
        else:
            logger.error('Game with UUID: %s not found from redis.', idx)
            bottle.response.status = 404
            return
    else:
        if _fake_db.get(idx):
            bottle.response.status = 200
            return _fake_db[idx]
        else:
            logger.error('Game with UUID: %s not found from cache.', idx)
            bottle.response.status = 404
            return


@bottle.post(ROOT_URL_PATH)
def post_action():
    data = str(bottle.request.json.get(GAME_BOARD_STATUS, '')).upper()
    data = DEFAULT_EMPTY_INPUT if data == '' else data
    logger.debug('POST: %s', data)
    if data:
        if validate_input(data):
            game_data = dict()
            user_symbol = user_start_game(data)
            game_sn = str(uuid.uuid4())
            if not user_symbol:
                logger.debug('Server start game >>>')
                user_symbol = CROSSES if server_default_symbol == NOUGHTS else NOUGHTS
                data, _ = server_step(data, user_symbol)
                logger.debug('Server start game with %s', data)
                game_data = {
                    GAME_BOARD_STATUS: data,
                    USER_SYMBOL: user_symbol,
                    STATUS: RUNNING
                }
            elif user_symbol == 500:
                bottle.response.status = 500
            else:
                logger.debug('User start game >>>')
                data, _ = server_step(data, user_symbol)
                logger.debug('Server response game with %s', data)
                game_data = {
                    GAME_BOARD_STATUS: data,
                    USER_SYMBOL: user_symbol,
                    STATUS: RUNNING
                }

            if _fake_db is None:  # redis in use
                redis_client.set(game_sn, json.dumps(game_data))
            else:
                _fake_db[game_sn] = game_data
            bottle.response.status = 201
            bottle.response.set_header('Location', ROOT_URL_PATH + game_sn)
            return game_data
        else:
            bottle.response.status = '400 Invalid Position data'
    else:
        logger.exception('Received Non-JSON format data')
        bottle.response.status = 415
    return


@bottle.put(ROOT_URL_PATH + '<idx>')
def update_action(idx):
    data = str(bottle.request.json.get(GAME_BOARD_STATUS)).upper()
    logger.debug('PUT: %s >>> %s', idx, data)
    if data:
        if validate_input(data):
            if _fake_db is None:  # redis in use
                data_from_redis = redis_client.get(idx)
                if data_from_redis:
                    old_game_data = json.loads(data_from_redis)
                else:
                    old_game_data = None
            else:
                old_game_data = _fake_db.get(idx)

            if old_game_data:
                if old_game_data.get(STATUS) == RUNNING:
                    logger.debug('Fetched existing game board data: %s', pprint.pformat(old_game_data))
                    old_game_board_status = old_game_data.get(GAME_BOARD_STATUS)
                    result, user_changed_symbol, user_changed_idx = validate_new_input(old_game_board_status, data)
                    if result:
                        user_symbol = old_game_data.get(USER_SYMBOL)
                        if user_changed_symbol == user_symbol:
                            game_status = RUNNING
                            bottle.response.status = 201
                            logger.debug('User continue game with %s', data)
                            if check_winner(data, user_symbol, user_changed_idx):
                                # if user is the winner, then set the game status according to user's symbol
                                game_status = X_WON if user_symbol == CROSSES else O_WON
                            if len(str(data).translate(None, EMPTY)) == MAX_ALLOW_DATA_LENGTH:
                                # if game board reach max length, then this is a draw game
                                game_status = DRAW
                            if game_status == RUNNING:
                                logger.debug('User not win, Server continue....')
                                data, move_idx = server_step(data, user_symbol)
                                logger.debug('Server response game with %s', data)
                                if check_winner(data, NOUGHTS if user_symbol == CROSSES else CROSSES, move_idx):
                                    # if server is the winner, then set the game status according to server's symbol
                                    game_status = O_WON if user_symbol == CROSSES else X_WON
                                if len(str(data).translate(None, EMPTY)) == MAX_ALLOW_DATA_LENGTH:
                                    # if game board reach max length, then this is a draw game
                                    game_status = DRAW
                            else:
                                logger.info('Game end with game_status: %s', game_status)
                            game_data = {
                                GAME_BOARD_STATUS: data,
                                USER_SYMBOL: user_symbol,
                                STATUS: game_status
                            }
                            if _fake_db is None:  # redis in use
                                redis_client.set(idx, json.dumps(game_data))
                            else:
                                _fake_db[idx] = game_data
                            bottle.response.set_header('Location', ROOT_URL_PATH + idx)
                            return game_data
                        else:
                            bottle.response.status = '400 Mismatched user move. Was: %s, but not: %s' % (
                                user_symbol, user_changed_symbol)
                    else:
                        bottle.response.status = '400 Invalid input data %s' % data
                else:
                    bottle.response.status = '400 Game already finished with game_status: %s' % old_game_data.get(STATUS)
            else:
                logger.error('Game with UUID: %s not found.', idx)
                bottle.response.status = 404
        else:
            bottle.response.status = '400 Invalid input data %s' % data
    else:
        logger.exception('Received Non-JSON format data')
        bottle.response.status = 415
    return


@bottle.delete(ROOT_URL_PATH + '<idx>')
def delete_action(idx):
    logger.debug('DELETE: %s', idx)

    if _fake_db is None:  # redis in use
        if redis_client.delete(idx):
            bottle.response.status = 204
            return
        else:
            logger.error('Item with idx: %s not found from redis.', idx)
            bottle.response.status = 404
            return
    else:
        if _fake_db.get(idx):
            del _fake_db[idx]
            bottle.response.status = 204
            return
        else:
            logger.error('Item with idx: %s not found from local cache.', idx)
            bottle.response.status = 404
            return


@bottle.delete(TEST_URL_PATH + '<delete_authentication>')
# For testing purpose only
def delete_action(delete_authentication):
    if delete_authentication == 'UnitTest_only':
        if _fake_db is None:  # redis in use
            redis_client.flushall()
        else:
            _fake_db.clear()
        bottle.response.status = 204
        return
    else:
        bottle.response.status = 400
        return 'Invalid request! Provide Delete Authentication String in order to remove all Data. FOR TESTING ONLY'


def server_start(ip, port):
    bottle.run(host=ip, port=port)


if __name__ == '__main__':
    bind_ip = '0.0.0.0'
    bind_port = 3000
    global server_default_symbol
    try:
        if len(sys.argv) < 2:
            logger.info('No given bind IP and port, use default value as: %s:%s', bind_ip, bind_port)
        elif len(sys.argv) == 2:
            bind_ip = sys.argv[1]
            logger.info('Override bind IP to: %s', bind_ip)
        else:
            bind_ip = sys.argv[1]
            bind_port = sys.argv[2]
            server_default_symbol = NOUGHTS if sys.argv[3] == NOUGHTS else CROSSES
            logger.info('Override bind IP and port to: %s:%s', bind_ip, bind_port)
            logger.info('Server symbol is %s', server_default_symbol)
    except Exception as e:
        logger.exception('Exception during argument value handling: %s, start with default value: %s:%s', e.message, bind_ip, bind_port)

    logger.info('Starting server with %s:%s', bind_ip, bind_port)
    server_start(bind_ip, bind_port)
