#!/usr/bin/env python
import logging
import pprint
import subprocess
import sys
import unittest
import uuid

import requests

from tictactoe_restful_backend import user_start_game, GAME_BOARD_STATUS, server_step, USER_SYMBOL, \
    NOUGHTS, CROSSES, validate_new_input, validate_input, find_diff_count, check_winner, STATUS, server_default_symbol, \
    DEFAULT_EMPTY_INPUT, RUNNING, X_WON, O_WON, DRAW

APP_NAME = 'test_tictactoe_restful_backend'
FORMAT = '%(asctime)s %(module)s:%(lineno)-3s[%(levelname)s] %(message)s'

logging.basicConfig(filename='testing.log', format=FORMAT, level=logging.WARN)
logger = logging.getLogger(APP_NAME)
logger.setLevel(logging.ERROR)

EXPECTED_FAIL_REASON = 'Invalid Position data'
REST_SERVER_PORT = 3000
V2_URL = 'http://%s:%s/api/v2/'
ROOT_URL = 'http://%s:%s/'
NON_EXIST_URL = 'http://%s:%s/something/notexist/'
LOCAL_REST_URL = 'http://%s:%s/api/v1/games/'
TEST_REST_URL = 'http://%s:%s/tst/v1/games/'
RESET_DATA_AUTH_STRING = 'UnitTest_only'


def user_step(data, user_symbol):
    server_symbol = NOUGHTS if user_symbol == CROSSES else CROSSES
    game_data, move_index = server_step(data, server_symbol)
    return game_data


def reset_old_data():
    try:
        r = requests.delete(TEST_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + RESET_DATA_AUTH_STRING)
        logger.debug('Return Code for resetting server existing data: %s.', r.status_code)
    except Exception as e:
        logger.exception('Failed to reset all existing data: %s', e.message)


def check_and_start_rest_server():
    r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
    logger.debug('Return Code for querying server initialized data: %s, data: %s', r.status_code, r.json())
    if r.status_code == 404 or r.status_code == 400:
        try:
            subprocess.check_call('/opt/script/tictactoe/tictactoe_restful_backend.py 127.0.0.1 3000', shell=True)
        except Exception as e:
            logger.exception('Exception occurred during start simple REST server: %s', e.message)


class SimpleRestServerTestCase(unittest.TestCase):
    def setUp(self):
        reset_old_data()
        check_and_start_rest_server()

        self.game_one_uuid = ''
        self.game_two_uuid = ''
        self.game_three_uuid = str(uuid.uuid4())
        self.game_four_uuid = ''
        self.game_one_data = dict()
        self.game_two_data = dict()
        self.game_three_data = dict()
        self.game_four_data = dict()

    def tearDown(self):
        pass

    def test_user_start_game(self):
        self.assertEqual(user_start_game('X--------'), CROSSES)
        self.assertEqual(user_start_game('O--------'), NOUGHTS)
        self.assertEqual(user_start_game('x--------'), CROSSES)
        self.assertEqual(user_start_game('o--------'), NOUGHTS)
        self.assertEqual(user_start_game('---------'), 0)
        self.assertEqual(user_start_game(''), 0)
        self.assertEqual(user_start_game(None), 0)

    def test_validate_input(self):
        self.assertTrue(validate_input('X--------'))
        self.assertFalse(validate_input('X---------'))
        self.assertFalse(validate_input('XA-------'))
        self.assertFalse(validate_input('X-------'))

    def test_server_step(self):
        old_game_data = 'X--------'
        new_game_data, _ = server_step(old_game_data, CROSSES)
        result, changed_symbol, changed_idx = find_diff_count(old_game_data, new_game_data)
        self.assertEqual((result, changed_symbol), (True, NOUGHTS))
        old_game_data = 'XO-------'
        new_game_data, _ = server_step(old_game_data, NOUGHTS)
        result, changed_symbol, changed_idx = find_diff_count(old_game_data, new_game_data)
        self.assertEqual((result, changed_symbol), (True, CROSSES))
        # This is valid during UT, as game board data validation is in upper level
        old_game_data = 'OO-------'
        new_game_data, _ = server_step(old_game_data, NOUGHTS)
        result, changed_symbol, changed_idx = find_diff_count(old_game_data, new_game_data)
        self.assertEqual((result, changed_symbol), (True, CROSSES))

    def test_find_diff_count(self):
        old_game_data = 'XO-------'
        new_game_data = 'XOX------'
        self.assertEqual(find_diff_count(old_game_data, new_game_data), (1, CROSSES, 2))
        old_game_data = 'XOO------'
        new_game_data = 'XOOX-----'
        self.assertEqual(find_diff_count(old_game_data, new_game_data), (1, CROSSES, 3))
        old_game_data = 'XOX------'
        new_game_data = 'XOX--O---'
        self.assertEqual(find_diff_count(old_game_data, new_game_data), (1, NOUGHTS, 5))

    def test_validate_new_input(self):
        self.assertEqual(validate_new_input('X--------', 'XO-------'), (True, NOUGHTS, 1))
        self.assertEqual(validate_new_input('X--------', 'X--------'), (False, None, None))
        self.assertEqual(validate_new_input('X--------', 'XOX-------'), (False, None, None))
        self.assertEqual(validate_new_input('X--------', 'XA-------'), (False, None, None))
        self.assertEqual(validate_new_input('X--------', 'X---O----'), (True, NOUGHTS, 4))
        # This input is valid during method level testing, as validation is in above level
        self.assertEqual(validate_new_input('X--------', 'X-----X--'), (True, CROSSES, 6))

    def test_check_winner(self):
        self.assertTrue(check_winner('XXX------', 'X', 0))
        self.assertTrue(check_winner('XXX------', 'X', 1))
        self.assertTrue(check_winner('XXX------', 'X', 2))
        self.assertFalse(check_winner('OOO------', 'X', 0))
        self.assertFalse(check_winner('OOO------', 'X', 2))
        self.assertFalse(check_winner('OOO------', 'X', 4))
        self.assertFalse(check_winner('OOO------', 'X', 6))
        self.assertFalse(check_winner('OOO------', 'X', 8))
        self.assertTrue(check_winner('OOO------', 'O', 0))
        self.assertTrue(check_winner('OOO------', 'O', 1))
        self.assertTrue(check_winner('OOO------', 'O', 2))
        self.assertFalse(check_winner('XXX------', 'O', 0))
        self.assertFalse(check_winner('XOOOXXXXO', 'X', 4))

    def test_combined_basic_query(self):
        self.get_all_games_as_empty()
        self.user_start_game_one_succ()
        self.get_one_game_succ(self.game_one_uuid, self.game_one_data)
        self.get_all_games_as_one_game()
        self.user_start_game_two_succ()
        self.get_one_game_succ(self.game_two_uuid, self.game_two_data)
        self.get_all_games_as_two_game()
        self.user_continue_with_game_one_succ()
        self.get_one_game_succ(self.game_one_uuid, self.game_one_data)
        self.user_start_game_three_fail_due_to_wrong_data()
        self.get_one_game_fail_not_exist(self.game_three_uuid)
        self.server_start_game_four_succ()
        self.get_one_game_succ(self.game_four_uuid, self.game_four_data)
        self.get_all_games_as_three_game()
        self.delete_one_game_succ(self.game_one_uuid)
        self.get_all_games_as_two_game()
        self.delete_one_game_failed_not_exist(self.game_one_uuid)
        self.get_all_games_as_two_game()
        self.get_one_game_fail_not_exist(self.game_one_uuid)
        self.user_continue_one_game_fail_due_to_wrong_data(self.game_two_uuid)
        self.user_continue_one_game_failed_due_to_not_exist(self.game_three_uuid)
        self.delete_one_game_failed_not_exist(self.game_three_uuid)
        self.get_all_games_as_two_game()
        self.query_unsupported_url()

    def test_game_from_user_start_to_end(self):
        game_data = {GAME_BOARD_STATUS: 'X--------'}
        self.game_from_user_start_to_end(game_data, NOUGHTS)

    def test_multiple_game_from_user_start_to_end(self):
        game_data_list = [
            (NOUGHTS, 'X--------'),
            (CROSSES, '-O-------'),
            (NOUGHTS, '--X------'),
            (CROSSES, '---O-----'),
            (NOUGHTS, '----X----'),
            (CROSSES, '-----O---'),
            (NOUGHTS, '------X--'),
            (CROSSES, '-------O-'),
            (NOUGHTS, '--------X')
        ]
        for user_symbol, game_board_data in game_data_list:
            self.game_from_user_start_to_end({GAME_BOARD_STATUS: game_board_data}, user_symbol)

    def test_game_from_server_start_to_end(self):
        self.game_from_server_start_to_end()

    def test_hundred_games_from_server_start_to_end(self):
        for x in range(0, 100):
            self.game_from_server_start_to_end()

    def test_continue_game_after_end(self):
        logger.debug('Server start a game .....')
        game_data = {GAME_BOARD_STATUS: ''}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=game_data)
        game_data = {GAME_BOARD_STATUS: DEFAULT_EMPTY_INPUT} if game_data.get(GAME_BOARD_STATUS) == '' else game_data
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), game_data.get(GAME_BOARD_STATUS))
        result, changed_symbol, changed_idx = validate_new_input(game_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, server_default_symbol)
        self.assertTrue(changed_idx in range(0, 9))
        while True:
            game_uuid = r.headers['location'].split('/')[-1]
            new_game_data = r.json()
            new_game_data[GAME_BOARD_STATUS] = user_step(r.json().get(GAME_BOARD_STATUS), r.json().get(USER_SYMBOL))
            r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=new_game_data)
            if r.json().get(STATUS) != RUNNING:
                logger.debug('Game end with game_status as %s and game board is: %s', r.json().get(STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertTrue(r.json().get(STATUS) == X_WON or r.json().get(STATUS) == O_WON or r.json().get(STATUS) == DRAW)
                break
            else:
                self.assertEqual(r.status_code, 201)
                self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), new_game_data.get(GAME_BOARD_STATUS))
                result, changed_symbol, changed_idx = validate_new_input(new_game_data.get(GAME_BOARD_STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertEqual(result, True)
                self.assertEqual(changed_symbol, server_default_symbol)
                self.assertTrue(changed_idx in range(0, 9))
                self.assertEqual(r.headers['location'].split('/')[-1], game_uuid)

        new_game_data = r.json()
        # continue send new step after game finished
        r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=new_game_data)
        self.assertEqual('Game already finished with game_status: %s' % new_game_data.get(STATUS), str(r.reason))
        self.assertEqual(r.status_code, 400)

    def game_from_user_start_to_end(self, game_data, server_symbol):
        logger.debug('User start a game .....')
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=game_data)
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), game_data.get(GAME_BOARD_STATUS))
        result, changed_symbol, changed_idx = validate_new_input(game_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, server_symbol)
        self.assertTrue(changed_idx in range(0, 9))
        while True:
            game_uuid = r.headers['location'].split('/')[-1]
            new_game_data = r.json()
            new_game_data[GAME_BOARD_STATUS] = user_step(r.json().get(GAME_BOARD_STATUS), r.json().get(USER_SYMBOL))
            r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=new_game_data)
            if r.json().get(STATUS) != RUNNING:
                logger.debug('Game end with status of %s and game board is: %s', r.json().get(STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertTrue(r.json().get(STATUS) == X_WON or r.json().get(STATUS) == O_WON or r.json().get(STATUS) == DRAW)
                break
            else:
                self.assertEqual(r.status_code, 201)
                self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), new_game_data.get(GAME_BOARD_STATUS))
                result, changed_symbol, changed_idx = validate_new_input(new_game_data.get(GAME_BOARD_STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertEqual(result, True)
                self.assertEqual(changed_symbol, server_symbol)
                self.assertTrue(changed_idx in range(0, 9))
                self.assertEqual(r.headers['location'].split('/')[-1], game_uuid)

    def game_from_server_start_to_end(self):
        logger.debug('Server start a game .....')
        game_data = {GAME_BOARD_STATUS: ''}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=game_data)
        game_data = {GAME_BOARD_STATUS: DEFAULT_EMPTY_INPUT} if game_data.get(GAME_BOARD_STATUS) == '' else game_data
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), game_data.get(GAME_BOARD_STATUS))
        result, changed_symbol, changed_idx = validate_new_input(game_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, server_default_symbol)
        self.assertTrue(changed_idx in range(0, 9))
        while True:
            game_uuid = r.headers['location'].split('/')[-1]
            new_game_data = r.json()
            new_game_data[GAME_BOARD_STATUS] = user_step(r.json().get(GAME_BOARD_STATUS), r.json().get(USER_SYMBOL))
            r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=new_game_data)
            if r.json().get(STATUS) != RUNNING:
                logger.debug('Game end with status of %s and game board is: %s', r.json().get(STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertTrue(r.json().get(STATUS) == X_WON or r.json().get(STATUS) == O_WON or r.json().get(STATUS) == DRAW)
                break
            else:
                self.assertEqual(r.status_code, 201)
                self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), new_game_data.get(GAME_BOARD_STATUS))
                result, changed_symbol, changed_idx = validate_new_input(new_game_data.get(GAME_BOARD_STATUS), r.json().get(GAME_BOARD_STATUS))
                self.assertEqual(result, True)
                self.assertEqual(changed_symbol, server_default_symbol)
                self.assertTrue(changed_idx in range(0, 9))
                self.assertEqual(r.headers['location'].split('/')[-1], game_uuid)

    def get_all_games_as_empty(self):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.json()), 0)

    def get_all_games_as_one_game(self):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.json()), 1)

    def get_all_games_as_two_game(self):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.json()), 2)

    def get_all_games_as_three_game(self):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.json()), 3)

    def user_start_game_one_succ(self):
        logger.debug('------------------ Start Game 1 ---------------------')
        self.game_one_data = {GAME_BOARD_STATUS: 'X--------'}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=self.game_one_data)
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), self.game_one_data.get(GAME_BOARD_STATUS))
        # return string contain one more 'O' there
        result, changed_symbol, changed_idx = validate_new_input(self.game_one_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, NOUGHTS)
        self.assertTrue(changed_idx in range(0, 9))
        self.game_one_uuid = r.headers['location'].split('/')[-1]
        self.game_one_data = r.json()
        logger.debug('Game 1 done with UUID: %s and Game data: %s', self.game_one_uuid, pprint.pformat(self.game_one_data))

    def user_continue_with_game_one_succ(self):
        logger.debug('------------------ Try to start Game 1 ---------------------')
        self.game_one_data[GAME_BOARD_STATUS] = user_step(self.game_one_data.get(GAME_BOARD_STATUS), self.game_one_data.get(USER_SYMBOL))
        logger.debug('Game 1 continue with: %s', pprint.pformat(self.game_one_data))
        r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + self.game_one_uuid, json=self.game_one_data)
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), self.game_one_data.get(GAME_BOARD_STATUS))
        # return string contain one more 'O' there
        result, changed_symbol, changed_idx = validate_new_input(self.game_one_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, NOUGHTS)
        self.assertTrue(changed_idx in range(0, 9))
        self.assertEqual(r.headers['location'].split('/')[-1], self.game_one_uuid)
        self.game_one_data = r.json()
        logger.debug('Game 1 started with UUID: %s and Game data: %s', self.game_one_uuid, pprint.pformat(self.game_one_data))

    def user_get_game_three_failed(self):
        logger.debug('------------------ Try to get Game 3 ---------------------')
        logger.debug('Game 3 continue with: %s', pprint.pformat(self.game_one_data))
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + self.game_three_uuid)
        self.assertEqual(r.status_code, 404)

    def user_continue_one_game_failed_due_to_not_exist(self, game_uuid):
        logger.debug('------------------ Try to continue Game with UUID: %s ---------------------', game_uuid)
        self.game_three_data[GAME_BOARD_STATUS] = user_step(self.game_one_data.get(GAME_BOARD_STATUS), self.game_one_data.get(USER_SYMBOL))
        logger.debug('Game %s continue with: %s', game_uuid, pprint.pformat(self.game_one_data))
        r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=self.game_three_data)
        self.assertEqual(r.status_code, 404)

    def user_start_game_two_succ(self):
        logger.debug('------------------ Try to start Game 2 ---------------------')
        self.game_two_data = {GAME_BOARD_STATUS: '----O----'}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=self.game_two_data)
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), self.game_two_data.values()[0])
        # return string contain one more 'X' there
        result, changed_symbol, changed_idx = validate_new_input(self.game_two_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, CROSSES)
        self.assertTrue(changed_idx in range(0, 9))
        self.game_two_uuid = r.headers['location'].split('/')[-1]
        self.game_two_data = r.json()
        logger.debug('Game 2 started with UUID: %s and Game data: %s', self.game_two_uuid, pprint.pformat(self.game_two_data))

    def user_start_game_three_fail_due_to_wrong_data(self):
        logger.debug('------------------ Try to start Game 3 ---------------------')
        self.game_three_data = {GAME_BOARD_STATUS: '----A----'}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=self.game_three_data)
        self.assertEqual(r.status_code, 400)
        logger.debug('Game 3 failed to start with return status code %s', r.status_code)

    def server_start_game_four_succ(self):
        logger.debug('------------------ Try to start Game 4 ---------------------')
        server_default_symbol = CROSSES
        self.game_four_data = {GAME_BOARD_STATUS: '---------'}
        r = requests.post(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT), json=self.game_four_data)
        self.assertEqual(r.status_code, 201)
        self.assertNotEqual(r.json().get(GAME_BOARD_STATUS), self.game_four_data.get(GAME_BOARD_STATUS))
        # return string contain one more 'X' there
        result, changed_symbol, changed_idx = validate_new_input(self.game_four_data[GAME_BOARD_STATUS], r.json().get(GAME_BOARD_STATUS))
        self.assertEqual(result, True)
        self.assertEqual(changed_symbol, server_default_symbol)
        self.assertTrue(changed_idx in range(0, 9))
        self.game_four_uuid = r.headers['location'].split('/')[-1]
        self.game_four_data = r.json()
        logger.debug('Game 4 started with UUID: %s and Game data: %s', self.game_four_uuid, pprint.pformat(self.game_four_data))

    def get_one_game_succ(self, game_uuid, game_data):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid)
        logger.debug('Get Game UUID: %s with data: %s', game_uuid, r.json())
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json(), game_data)

    def delete_one_game_succ(self, game_uuid):
        r = requests.delete(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid)
        self.assertEqual(r.status_code, 204)

    def delete_one_game_failed_not_exist(self, game_uuid):
        r = requests.delete(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid)
        self.assertEqual(r.status_code, 404)

    def get_one_game_fail_not_exist(self, game_uuid):
        r = requests.get(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid)
        self.assertEqual(r.status_code, 404)

    def user_continue_one_game_fail_due_to_wrong_data(self, game_uuid):
        logger.debug('------------------ Try to continue Game 2 ---------------------')
        invalid_game_data = dict()
        # Too long input game board data length
        invalid_game_data[GAME_BOARD_STATUS] = user_step(self.game_one_data.get(GAME_BOARD_STATUS), self.game_one_data.get(USER_SYMBOL)) + '-'
        logger.debug('Game 2 continue with: %s', pprint.pformat(self.game_one_data))
        r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=invalid_game_data)
        self.assertEqual(r.status_code, 400)

        # More than one move
        invalid_game_data[GAME_BOARD_STATUS] = user_step(self.game_one_data.get(GAME_BOARD_STATUS), self.game_one_data.get(USER_SYMBOL)) + USER_SYMBOL
        logger.debug('Game 2 continue with: %s', pprint.pformat(self.game_one_data))
        r = requests.put(LOCAL_REST_URL % (REST_SERVER_IP, REST_SERVER_PORT) + game_uuid, json=invalid_game_data)
        self.assertEqual(r.status_code, 400)

    def query_unsupported_url(self):
        r = requests.get(V2_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 404)

        r = requests.delete(ROOT_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 404)

        r = requests.post(NON_EXIST_URL % (REST_SERVER_IP, REST_SERVER_PORT))
        self.assertEqual(r.status_code, 404)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        REST_SERVER_IP = sys.argv.pop()
    else:
        REST_SERVER_IP = '127.0.0.1'

    unittest.main()
