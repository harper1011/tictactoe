FROM python:2.7
MAINTAINER Dapeng Jiao <harper1011@gmail.com>

WORKDIR /opt/script/tictactoe/

COPY SOURCES/*.py /opt/script/tictactoe/
COPY SOURCES/requirements.txt /opt/script/tictactoe/
RUN pip install --upgrade pip && pip install -r /opt/script/tictactoe/requirements.txt

EXPOSE 3000

HEALTHCHECK --interval=10s --timeout=2s CMD curl -f http://localhost:3000/api/v1/games/ || exit 1
ENTRYPOINT python /opt/script/tictactoe/tictactoe_restful_backend.py '0.0.0.0' 3000